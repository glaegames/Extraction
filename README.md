# Extraction
FPS stealth game made in Unreal Engine 4 where the player must steal the all powerful green orb and get it to the 'extraction zone'.

![](Extraction.mp4)
