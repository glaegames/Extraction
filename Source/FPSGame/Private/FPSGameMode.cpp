// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "FPSGameMode.h"
#include "FPSHUD.h"
#include "FPSCharacter.h"
#include "FPSGameState.h"
#include "Perception/PawnSensingComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"

AFPSGameMode::AFPSGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Blueprints/BP_Player"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AFPSHUD::StaticClass();

	// use custom game state class for multiplayer
	GameStateClass = AFPSGameState::StaticClass();
}

void AFPSGameMode::CompleteMission(APawn* InstigatorPawn, bool bMissionSuccess)
{
	if (InstigatorPawn)
	{
		if (SpectatingViewpointClass)
		{
			TArray<AActor*> ReturnedActors;
			UGameplayStatics::GetAllActorsOfClass(this, SpectatingViewpointClass, ReturnedActors);

			// Change view target if any valid actor found eg. spectating camera
			if (ReturnedActors.Num() > 0)
			{
				AActor* NewViewTarget = ReturnedActors[0];
				
				// Iterate over all the player controllers and change camera for each player
				for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; It++)
				{
					APlayerController* PlayerController = It->Get();
					if (PlayerController)
					{
						PlayerController->SetViewTargetWithBlend(NewViewTarget, 0.5f, EViewTargetBlendFunction::VTBlend_Cubic);
					}
				}
			}

			TArray<AActor*> ReturnedGuards;
			UGameplayStatics::GetAllActorsOfClass(this, AIGuardClass, ReturnedGuards);

			// Stop guards patrolling if any are found
			if (ReturnedGuards.Num() > 0)
			{
				for (AActor* Guard : ReturnedGuards)
				{
					ACharacter* Character = Cast<ACharacter>(Guard);
					
					// Stop movement if patrolling
					AController* Controller = Character->GetController();
					if (Controller)
					{
						Controller->StopMovement();
					}

					// Stop sensing player
					UActorComponent* PawnSensingComp = Character->GetComponentByClass(UPawnSensingComponent::StaticClass());
					if (PawnSensingComp)
					{
						PawnSensingComp->Deactivate();
					}

				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("SpectatingViewpointClass is nullptr. Please update GameMode class with valid subclass. Cannot change spectating view target."));
		}
	}

	AFPSGameState* GameState = GetGameState<AFPSGameState>();
	if (GameState)
	{
		GameState->MulticastOnMissionComplete(InstigatorPawn, bMissionSuccess);
	}

	OnMissionCompleted(InstigatorPawn, bMissionSuccess);

}
